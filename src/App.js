import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import MapView from 'react-native-maps';
import Routes from './Routes';
import LoginScreen from './screens/LoginScreen';

export default class App extends Component {
    render() {
        return (
            <Routes />
        );
    }
}
